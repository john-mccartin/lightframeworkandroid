package org.lightframework.demo.ui.main.dependencyinjection
import android.view.View

class ConcreteTestClass {
    val testInjectable : View? = null
}