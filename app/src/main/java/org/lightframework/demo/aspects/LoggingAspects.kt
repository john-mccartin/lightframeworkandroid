@file:Suppress("unused")

package org.lightframework.aspects

import android.util.Log
import android.view.View
import android.widget.TextView
/*
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.annotation.Pointcut
*/

/**
 * Aspect file used to add logging logic at compile time to common actions
 */
/*
@Aspect
class LoggingAspects {

    private val tag : String = "LoggingAspects"

    /**
     * Indicates that the execution of the onCreate method with a single Bundle argument is
     * called from any class in my Activity package
     */
    @Pointcut("execution(* *.onCreate(android.os.Bundle))")
    fun onCreate() {
        Log.d(tag, "onCreate() called")
    }

    /**
     * Indicates that the execution of the onStart method with a single Bundle argument is
     * called from any class in my Activity package
     */
    @Pointcut("execution(* *.onStart())")
    fun onStart() {
        Log.d(tag, "onStart() called")
    }

    /**
     * Indicates that the execution of the onResume method with a single Bundle argument is
     * called from any class in my Activity package
     */
    @Pointcut("execution(* *.onResume())")
    fun onResume() {
        Log.d(tag, "onResume() called")
    }

    /**
     * Runs the code in this method before the onCreate method gets called.
     *
     * Note that the value in the annotation is the same as the pointcut in this file that I want to use
     */
    @Before("onCreate()")
    fun onCreateAdvice(joinPoint: JoinPoint?) {
        if (joinPoint?.getThis() != null) {
            Log.d(tag,"onCreate called in " + joinPoint.getThis().javaClass.simpleName)
        }
    }

    @Before("onStart()")
    fun onStart(joinPoint: JoinPoint?) {
        if (joinPoint?.getThis() != null) {
            Log.d(tag,"onStart called in " + joinPoint.getThis().javaClass.simpleName)
        }
    }

    @Before("onResume()")
    fun onResume(joinPoint: JoinPoint?) {
        if (joinPoint?.getThis() != null) {
            Log.d(tag,"onResume called in " + joinPoint.getThis().javaClass.simpleName)
        }
    }

    /**
     * Captures the execution of any method named onClick in any class that has a return type of void.
     */
    @Pointcut("execution(void *.onClick(..))")
    fun onButtonClick() {
        Log.d(tag, "onClick() called")
    }

    /**
     * Runs the code in this method before the onCreate method gets called.
     *
     * The value in the annotation mentions both the pointcut to use and the argument that I want to capture.
     */
    @Before("onButtonClick() && args(view)")
    fun onClickAdvice(view: View?) {
        if (view is TextView) {
            val text = view.text.toString()
            Log.d(tag, text)
        }
    }
}
 */