package org.lightframework.annotation

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.FIELD)
annotation class InjectView(val viewId: Int)
