package org.lightframework.annotation.processor

import com.google.auto.service.AutoService
import com.squareup.kotlinpoet.*
import org.lightframework.annotation.InjectView
import java.io.File
import javax.annotation.processing.*
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement
import javax.lang.model.util.ElementFilter
import javax.tools.Diagnostic

@AutoService(Processor::class) // For registering the service
class BindFieldsProcessor : AbstractProcessor() {
    companion object {
        const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
    }

    override fun process(

        annotations: MutableSet<out TypeElement>?,
        roundEnv: RoundEnvironment?
    ): Boolean {

        message("Starting BindFieldsProcessor processing")

        if (roundEnv == null) {
            return false
        }

        /*
        roundEnv.getElementsAnnotatedWith(InjectView::class.java).forEach { fieldElement ->
            // Ensure that element kind is a Method
            if (fieldElement.kind != ElementKind.FIELD) {
                processingEnv.messager.printMessage(
                    Diagnostic.Kind.ERROR,
                    "Only fields can be annotated with @InjectView"
                )
                return false
            }
            processingEnv.messager.printMessage(Diagnostic.Kind.NOTE,
                "Found field" + fieldElement.kind.toString())
        }
        */
        return true
    }

    private fun message(message: String) {
        processingEnv.messager.printMessage(
            Diagnostic.Kind.NOTE,
            message
        )
    }

    /*private fun generateNewMethod(
        method: ExecutableElement,
        variable: VariableElement,
        packageOfMethod: String) {

        val generatedSourcesRoot: String =
            processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME].orEmpty()
        if (generatedSourcesRoot.isEmpty()) {
            processingEnv.messager.printMessage(
                Diagnostic.Kind.ERROR,
                "Can't find the target directory for generated Kotlin files.")
                return
        }

        val variableAsElement = processingEnv.typeUtils.asElement(variable.asType())
        val fieldsInArgument = ElementFilter.fieldsIn(variableAsElement.enclosedElements)
        val annotationArgs = method.getAnnotation(InjectView::class.java).viewIds

        val funcBuilder = FunSpec.builder("bindFields")
            .addModifiers(KModifier.PUBLIC)
            .addParameter(
                variable.simpleName.toString(),
                variableAsElement.asType().asTypeName()
            )
            .addParameter(
                method.getAnnotation(InjectView::class.java).viewName,
                ClassName("android.view", "View")
            )

        annotationArgs.forEachIndexed { index, viewId ->
            funcBuilder.addStatement(
                "%L.findViewById<%T>(R.id.%L).text = %L.%L",
                method.getAnnotation(InjectView::class.java).viewName,
                ClassName("android.widget", "TextView"),
                viewId,
                variable.simpleName,
                fieldsInArgument[index].simpleName
            )
        }
        val file = File(generatedSourcesRoot)
        file.mkdir()
        FileSpec.builder(packageOfMethod, "BindFieldsGenerated")
            .addFunction(funcBuilder.build()).build().writeTo(file)
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return mutableSetOf(InjectView::class.java.canonicalName)
    }*/
}