# Welcome
**Welcome to the Light Framework!**

The Light Framework is a framework created by Lighthouse Technology Group.  It's meant to aid
development with dependency injection and other platform specific automatons.  This repository
holds the Android version of the Light Framework.

The following feature are planned for the Android version:

* Dependency Injection
* View Binding
* Click Binding
* Fragment Binding
* Logging
* Analytics
* Web Service Binding
* Persistence w/caching
* Firebase Util
* GUI Dev Plugin
* Service Binding
* Security
* Recycler View Automation
* Util code including dialogs
* Login Component
* Enhanced View Model
---
## License
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0][1]

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]:http://www.apache.org/licenses/LICENSE-2.0
